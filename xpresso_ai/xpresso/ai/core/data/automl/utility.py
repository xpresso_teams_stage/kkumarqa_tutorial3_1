import pandas as pd
import os
from xpresso.ai.core.commons.utils.constants import EMPTY_STRING


def to_excel(output_path, filename, sheets=dict()):
    """Helper function to output datafram to excel
    Args:
        output_path('str"): path where the excel file is to be stored
        filename('str'): filename of the excel file
        sheets(:dict): dictionary with sheet name as key and dataframes as
        values
    """
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    writer = pd.ExcelWriter(os.path.join(output_path, filename),
                            engine='xlsxwriter')
    for sheet in sheets:
        sheets[sheet].to_excel(writer, sheet_name=sheet.upper())
    writer.save()


def get_file_from_path_string(path, file_name):
    """
    Get file name from path if exists
    Args:
        path(str): path to be split
        file_name(str): file name if explicitly provided
    Returns:
         Processed output path and filename
    """
    output_file_name = os.path.basename(path)
    output_path = os.path.dirname(path)
    if file_name:
        output_file_name = file_name
    output_path = os.path.abspath(os.path.join(os.getcwd(), output_path))
    return output_path, output_file_name


def set_extension(file_name, extension):
    """
    Set extension for filename
    Args:
        file_name(str): filename whose extension to be set
        extension(str): extension of the filename to be set
    Returns:
        Filename with correct extension
    """
    temp_filename, temp_ext = os.path.splitext(file_name)
    if not extension.startswith('.'):
        extension = ".{}".format(extension)
    if temp_ext == extension:
        return file_name
    return "{}{}".format(temp_filename, extension)
