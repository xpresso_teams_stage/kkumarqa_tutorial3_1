""" GoogleBigQueryConnector class for connectivity to GCP BigQuery """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'


import xpresso.ai.core.commons.utils.constants as constants
import xpresso.ai.core.data.connections.connector_exception as connector_exception
from xpresso.ai.core.data.connections.abstract_db import AbstractDBConnector
from xpresso.ai.core.data.connections.external.big_query import client as bigquery
from xpresso.ai.core.logging.xpr_log import XprLogger


class GoogleBigQueryConnector(AbstractDBConnector):
    """"""

    def __init__(self):
        """

        __init__() here initializes the client object needed for
        interacting with datasource API

        """

        self.client = None

    def getlogger(self):
        """

        Xpresso logger built on top of python module

        :return: xpresso logging module

        """

        return XprLogger()

    def connect(self, config):
        """

        Connect method to establish client-side connection with GCP BigQuery

        :param config: A JSON object, input by the user, that states the table
                        to be imported

        :return: table: target table name (String object)

        """

        self.client = bigquery.BigQueryConnector(config)
        table = config.get(constants.table)
        columns = config.get(constants.columns)
        return table, columns

    def import_files(self, config):
        """

        BQConnector does not support importing files

        :param config:

        :return:
        """

        return None

    def import_dataframe(self, config):
        """

        Importing data from a user specified table in BigQuery dataset

        :param config: A JSON object, input by the user, that states the table
                        to be imported

        Usage:

            config = {
                    "cred_path": "/home/abzooba/Downloads/My First Project-30e11a68c254.json",
                    "DSN": "testing",
                    "table": "posts_questions"
            }

        :return: data_frame: A DataFrame object of the required table
        """

        data_frame = None
        try:
            table, columns = self.connect(config)
            data_frame = self.client.import_data(table, columns)
            self.close()
            self.getlogger().info("Data imported from database.")
        except connector_exception.ConnectorImportFailed as exc:
            self.getlogger().exception("\nInput configurations might be wrong. "
                                       "Check input JSON object.\n\n%s" % str(exc))
        return data_frame

    def close(self):
        """

        Method to close connection to BigQuery

        :return: None

        """

        self.client.close()
